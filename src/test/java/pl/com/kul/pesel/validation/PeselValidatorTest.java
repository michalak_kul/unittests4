package pl.com.kul.pesel.validation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PeselValidatorTest {

    private PeselValidator peselValidator;

    @BeforeEach
    void setUp() {
        peselValidator = new PeselValidator();
    }

//    @Test
//    public void shouldReturnInvalidPeselResultWhenPeselIsNull() {
//        //given
//        String peselToValidate = null;
//        PeselValidationResult expectedResult =
//                PeselValidationResult.validationFailed(PeselValidationError.NULL_VALUE);
//        //when
//        PeselValidationResult peselValidationResult = peselValidator.validatePesel(peselToValidate);
//        //then
//        assertEquals(expectedResult, peselValidationResult);
//    }
//
//    @Test
//    public void shouldReturnIncorrectLengthWhenPeselIsTooShort() {
//        //given
//        String peselToValidate = "89";
//        PeselValidationResult expectedResult =
//                PeselValidationResult.validationFailed(PeselValidationError.INCORRECT_LENGTH);
//        //when
//        PeselValidationResult peselValidationResult = peselValidator.validatePesel(peselToValidate);
//        //then
//        assertEquals(expectedResult, peselValidationResult);
//    }
//
//    @Test
//    public void shouldReturnIllegalCharactersWhenPeselContainsLetters() {
//        //given
//        String peselToValidate = "aaaaaaaaaaa";
//        PeselValidationResult expectedResult =
//                PeselValidationResult.validationFailed(PeselValidationError.CONTAINS_ILLEGAL_CHARACTERS);
//        //when
//        PeselValidationResult peselValidationResult = peselValidator.validatePesel(peselToValidate);
//        //then
//        assertEquals(expectedResult, peselValidationResult);
//    }
//
//    @Test
//    public void shouldReturnIllegalChecksumWhenPeselIsIncorrect() {
//        //given
//        String peselToValidate = "44030505522";
//        PeselValidationResult expectedResult =
//                PeselValidationResult.validationFailed(PeselValidationError.CHECKSUM_FAILED);
//        //when
//        PeselValidationResult peselValidationResult = peselValidator.validatePesel(peselToValidate);
//        //then
//        assertEquals(expectedResult, peselValidationResult);
//    }
//
//    @Test
//    public void shouldReturnSuccessWhenPeselIsCorrect() {
//        //given
//        String peselToValidate = "44030505526";
//        PeselValidationResult expectedResult =
//                PeselValidationResult.successfulValidation();
//        //when
//        PeselValidationResult peselValidationResult = peselValidator.validatePesel(peselToValidate);
//        //then
//        assertEquals(expectedResult, peselValidationResult);
//    }

    @ParameterizedTest
    @MethodSource(value = "peselValidatorSource")
    public void shouldTestAllPaths(String peselToValidate, PeselValidationResult expectedResult) {
        //given
        //when
        PeselValidationResult peselValidationResult = peselValidator.validatePesel(peselToValidate);
        //then
        assertEquals(expectedResult, peselValidationResult);
    }

    public static Stream<Arguments> peselValidatorSource() {
        return Stream.of(
                Arguments.of(null, PeselValidationResult.validationFailed(PeselValidationError.NULL_VALUE)),
                Arguments.of("89", PeselValidationResult.validationFailed(PeselValidationError.INCORRECT_LENGTH)),
                Arguments.of("aaaaaaaaaaa", PeselValidationResult.validationFailed(PeselValidationError.CONTAINS_ILLEGAL_CHARACTERS)),
                Arguments.of("44030505522", PeselValidationResult.validationFailed(PeselValidationError.CHECKSUM_FAILED)),
                Arguments.of("44030505526", PeselValidationResult.successfulValidation())
        );
    }
}
