package pl.com.kul;

public class PeselValidationException extends RuntimeException {

    public PeselValidationException(String message) {
        super(message);
    }
}
