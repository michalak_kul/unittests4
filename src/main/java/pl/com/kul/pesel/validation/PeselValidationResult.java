package pl.com.kul.pesel.validation;

import java.util.Objects;

public class PeselValidationResult {
    private final boolean isPeselValid;
    private PeselValidationError validationError;

    private PeselValidationResult(boolean isPeselValid) {
        this.isPeselValid = isPeselValid;
    }

    private PeselValidationResult(boolean isPeselValid, PeselValidationError validationError) {
        this.isPeselValid = isPeselValid;
        this.validationError = validationError;
    }

    public static PeselValidationResult successfulValidation(){
        return new PeselValidationResult(true, null);
    }

    public static PeselValidationResult validationFailed(PeselValidationError peselValidationError){
        return new PeselValidationResult(false, peselValidationError);
    }

    public boolean isPeselValid() {
        return isPeselValid;
    }

    public PeselValidationError getValidationError() {
        return validationError;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PeselValidationResult that = (PeselValidationResult) o;
        return isPeselValid == that.isPeselValid && validationError == that.validationError;
    }

    @Override
    public int hashCode() {
        return Objects.hash(isPeselValid, validationError);
    }
}
