package pl.com.kul.pesel.validation;

public enum PeselValidationError {
    NULL_VALUE ("Pesel is null"),
    INCORRECT_LENGTH("Pesel have incorrect length"),
    CONTAINS_ILLEGAL_CHARACTERS("Pesel contains illegal characters"),
    CHECKSUM_FAILED("Pesel checksum failed");

    private final String message;

    PeselValidationError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
