package pl.com.kul;

enum PeselBirthDateParts {
    DAY(4,5), MONTH(2,3), YEAR(0,1);

    private final int firstIndex;
    private final int secondIndex;

    PeselBirthDateParts(int firstIndex, int secondIndex) {
        this.firstIndex = firstIndex;
        this.secondIndex = secondIndex;
    }

    int getFirstIndex() {
        return firstIndex;
    }

    int getSecondIndex() {
        return secondIndex;
    }
}
